//
//  PropertiesViewController.m
//  UIKitExamplesForPresentation
//
//  Created by GeraldSchuster on 2/18/14.
//  Copyright (c) 2014 Gerald Schuster. All rights reserved.
//

#import "PropertiesViewController.h"

@interface PropertiesViewController ()

@property (nonatomic, strong) UIDynamicAnimator * myAnimator;

@end

@implementation PropertiesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Initialize the animator and set the reference view
    self.myAnimator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    // create 3 squares for examples
    UIView *squareOne = [[UIView alloc] initWithFrame:CGRectMake(25, 80, 50, 50)];
    UIView *squareTwo = [[UIView alloc] initWithFrame:CGRectMake(125, 80, 50, 50)];
    UIView *squareThree = [[UIView alloc] initWithFrame:CGRectMake(225, 80, 50, 50)];
    
    // give squares colors
    squareOne.backgroundColor = [UIColor redColor];
    squareTwo.backgroundColor = [UIColor blueColor];
    squareThree.backgroundColor = [UIColor blackColor];

    // add the views
    [self.view addSubview:squareOne];
    [self.view addSubview:squareTwo];
    [self.view addSubview:squareThree];

    // Give the 3 blocks gravity
    UIGravityBehavior *myGravity = [[UIGravityBehavior alloc] initWithItems:@[squareOne, squareTwo ,squareThree]];

    // add gravity to the animator
    [self.myAnimator addBehavior:myGravity];
    
    
    // Give the 3 blocks collisions
    UICollisionBehavior *myCollision = [[UICollisionBehavior alloc] initWithItems:@[squareOne, squareTwo, squareThree]];
    
    // set the bounds into a boundry
    myCollision.translatesReferenceBoundsIntoBoundary = YES;
    
    // add myCollision to the animator
    [self.myAnimator addBehavior:myCollision];
    
    // DynamicItemBehaviors have different properties that can be set for many different combinations to have
    // many different effects on items.
    UIDynamicItemBehavior *behaviorOne = [[UIDynamicItemBehavior alloc] initWithItems:@[squareOne]];
    [behaviorOne setAllowsRotation:YES];
    [behaviorOne setDensity:2.0];
    [behaviorOne setElasticity:1.0];
    [behaviorOne setFriction:0.2];
    [behaviorOne setResistance:0.0];
    [behaviorOne setAngularResistance:0.0];
    
    // .75 elasticity for behaviorTwo
    UIDynamicItemBehavior *behaviorTwo = [[UIDynamicItemBehavior alloc] initWithItems:@[squareTwo]];
    [behaviorTwo setAllowsRotation:YES];
    [behaviorTwo setDensity:1.0];
    [behaviorTwo setElasticity:.75];
    [behaviorTwo setFriction:0.2];
    [behaviorTwo setResistance:0.0];
    [behaviorTwo setAngularResistance:0.0];
    
    // .1 elasticity for behaviorThree
    UIDynamicItemBehavior *behaviorThree = [[UIDynamicItemBehavior alloc] initWithItems:@[squareThree]];
    [behaviorThree setAllowsRotation:YES];
    [behaviorThree setDensity:1.0];
    [behaviorThree setElasticity:0.1];
    [behaviorThree setFriction:0.2];
    [behaviorThree setResistance:0.0];
    [behaviorThree setAngularResistance:0.0];
    
    // add the behaviors to the animator
    [self.myAnimator addBehavior:behaviorOne];
    [self.myAnimator addBehavior:behaviorTwo];
    [self.myAnimator addBehavior:behaviorThree];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
