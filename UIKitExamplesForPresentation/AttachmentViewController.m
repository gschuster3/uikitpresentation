//
//  AttachmentViewController.m
//  UIKitExamplesForPresentation
//
//  Created by GeraldSchuster on 2/20/14.
//  Copyright (c) 2014 Gerald Schuster. All rights reserved.
//

#import "AttachmentViewController.h"

@interface AttachmentViewController ()

@property (weak, nonatomic) IBOutlet UIView *anchor;
@property (nonatomic, strong) UIAttachmentBehavior *attachmenWithAnchor;
@property (nonatomic, strong) UIDynamicAnimator * myAnimator;

@end

@implementation AttachmentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Initialize the animator and set the reference view
    self.myAnimator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    // Create the views for attachments
    UIView *squareOne = [[UIView alloc] initWithFrame:CGRectMake(100, 100, 50, 50)];
    UIView *squareTwo = [[UIView alloc] initWithFrame:CGRectMake(40, 40, 50, 50)];
    
    
    // Color code the views
    squareOne.backgroundColor = [UIColor redColor];
    squareTwo.backgroundColor = [UIColor blueColor];
    
    
    //  Give the 2 squares gravity to allow them to fall to view attachment in action.
    UIGravityBehavior *gravity = [[UIGravityBehavior alloc] initWithItems:@[squareOne, squareTwo]];
    
    // Add collision so the squares hit the bounds
    UICollisionBehavior *collision = [[UICollisionBehavior alloc] initWithItems:@[squareOne, squareTwo]];
    
    // collide with bounds
    collision.translatesReferenceBoundsIntoBoundary = YES;
    
    // Add the views into the main view
    [self.view addSubview:squareOne];
    [self.view addSubview:squareTwo];
    
    
    // Initialize UIAttachmentBehavior.  Attach squareOne with squareTwo
    UIAttachmentBehavior *attachOne = [[UIAttachmentBehavior alloc] initWithItem:squareOne attachedToItem:squareTwo];
    
    // Set some properties of attachOne
    [attachOne setDamping:0.2];
    [attachOne setFrequency:1.0];
    
    // Initialize another UIAttachmentBehavior.  SquareTwo will be attached to an anchor point.
    // I put an anchor point in through storyboards, and connected it to the anchor property above.
    // The attachedToAnchor must give a CGPoint for the anchor point, and it was the only way I could get it working. So we are just hardcoding in the point where we placed the anchor View in the storyboard
    self.attachmenWithAnchor = [[UIAttachmentBehavior alloc] initWithItem:squareTwo attachedToAnchor:CGPointMake(150, 100)];
    
    
    // Add all of the behaviors to the animator
    [self.myAnimator addBehavior:gravity];
    [self.myAnimator addBehavior:collision];
    [self.myAnimator addBehavior:attachOne];
    [self.myAnimator addBehavior:self.attachmenWithAnchor];
    
}



- (IBAction)touchForAttachment:(UIGestureRecognizer *)gesture
{
    // This allows the user to tap the screen to set a new anchor point.  It also allows a user to drag the anchor point to a new location and view the attachment in progress.
    
    // When the screen is touched, set the anchor point of attachWithAnchor to the spot that was touched
    [self.attachmenWithAnchor setAnchorPoint:[gesture locationInView:self.view]];
    
    // Move the anchor to that spot
    self.anchor.center = self.attachmenWithAnchor.anchorPoint;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
