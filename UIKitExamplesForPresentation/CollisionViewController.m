//
//  CollisionViewController.m
//  UIKitExamplesForPresentation
//
//  Created by GeraldSchuster on 2/18/14.
//  Copyright (c) 2014 Gerald Schuster. All rights reserved.
//

#import "CollisionViewController.h"

@interface CollisionViewController ()

@property (nonatomic, strong) UIDynamicAnimator *myAnimator;

@end

@implementation CollisionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Initialize the animator and set the reference view
    self.myAnimator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    // Create the views for collisions
    UIView *redSquareOne = [[UIView alloc] initWithFrame:CGRectMake(100, 100, 100, 100)];
    UIView *blueSquare = [[UIView alloc] initWithFrame:CGRectMake(40, 40, 40, 40)];
    UIView *redBlock = [[UIView alloc] initWithFrame:CGRectMake(0, 300, 130, 20)];
    
    
    // Color code the views
    redSquareOne.backgroundColor = [UIColor redColor];
    blueSquare.backgroundColor = [UIColor blueColor];
    redBlock.backgroundColor = [UIColor redColor];
    
    //  Give the 2 squares gravity to allow them to fall. Keep the block stationary
    UIGravityBehavior *gravity = [[UIGravityBehavior alloc] initWithItems:@[redSquareOne, blueSquare]];
    
    // Add the views into the main view
    [self.view addSubview:redSquareOne];
    [self.view addSubview:redBlock];
    [self.view addSubview:blueSquare];
    
    
    // Add gravity to the dynamic animator
    [self.myAnimator addBehavior:gravity];
    
    
    // When red views collide, they should be noticed.
    // Initialize a  UICollisionBehavior, with the 2 red objects
    UICollisionBehavior *collisionRed = [[UICollisionBehavior alloc] initWithItems:@[redSquareOne, redBlock]];
    
    // We want all squares to stop when they reach the edge of the screen
    // translateReferenceBoundsIntoBoundary sets the bounds of the reference view into a boundry for collisions
    collisionRed.translatesReferenceBoundsIntoBoundary = YES;
    
    
    // Blue square should be able to pass through the red barrier, but not fall off the screen.
    // It needs its own collision behavior to recognize the edge of the screen
    UICollisionBehavior *collisionBlue = [[UICollisionBehavior alloc] initWithItems:@[blueSquare]];
    collisionBlue.translatesReferenceBoundsIntoBoundary = YES;
    
    // add a boundary that coincides with the top edge to give the appearance of a stationary object
    CGPoint rightEdge = CGPointMake(redBlock.frame.origin.x + redBlock.frame.size.width, redBlock.frame.origin.y);
    [collisionRed addBoundaryWithIdentifier:@"redBlock"
                                fromPoint:redBlock.frame.origin
                                  toPoint:rightEdge];

    
    // Add collisions to the dynamic animator
    [self.myAnimator addBehavior:collisionRed];
    [self.myAnimator addBehavior:collisionBlue];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
