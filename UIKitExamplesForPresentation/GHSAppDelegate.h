//
//  GHSAppDelegate.h
//  UIKitExamplesForPresentation
//
//  Created by GeraldSchuster on 2/18/14.
//  Copyright (c) 2014 Gerald Schuster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GHSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
