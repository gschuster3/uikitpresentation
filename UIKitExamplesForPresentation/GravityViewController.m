//
//  GravityViewController.m
//  UIKitExamplesPres
//
//  Created by GeraldSchuster on 2/18/14.
//  Copyright (c) 2014 Gerald Schuster. All rights reserved.
//

#import "GravityViewController.h"
#import "randomSquare.h"

@interface GravityViewController ()

@property (nonatomic, strong) UIDynamicAnimator * myAnimator;

@end

@implementation GravityViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    // Initialize the animator and set the reference view
    self.myAnimator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // touch event
    UITouch *touch = [touches anyObject];
    
    // get the point where the touch occured
    CGPoint point = [touch locationInView:self.view];
    
    // init a new random colored square
    randomSquare *newSquare = [[randomSquare alloc] init];
    
    // set the center of the square to the point that was touched
    newSquare.center = CGPointMake(point.x, point.y);
    
    
    // alloc and init the Gravity Behavior, and add the square view to it
    UIGravityBehavior *gravity = [[UIGravityBehavior alloc] initWithItems:@[newSquare]];
    
    // magnitude and angles are 2 properties of gravity
    
    // magnitude of 1.0 represents an acceleration of 1000 points / second squared
    //gravity.magnitude = 3.0;
    
    // angle, in radians, of the gravity.
    //gravity.angle = .5;

    
    // add the newSquare to the reference view  (self.view)
    [self.view addSubview:newSquare];
    
    // add the gravity behavior to the animator
    [self.myAnimator addBehavior:gravity];
}
@end
