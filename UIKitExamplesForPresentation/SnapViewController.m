//
//  SnapViewController.m
//  UIKitExamplesForPresentation
//
//  Created by GeraldSchuster on 2/18/14.
//  Copyright (c) 2014 Gerald Schuster. All rights reserved.
//

#import "SnapViewController.h"

@interface SnapViewController ()

@property (nonatomic, strong) UIDynamicAnimator * myAnimator;

@end

@implementation SnapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    // Initialize the animator and set the reference view
    self.myAnimator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    
    
}



- (IBAction)handleSnapGesture:(UITapGestureRecognizer*)gesture
{
    
    // get the point that the user touches in the view
    CGPoint point = [gesture locationInView:self.view];
    

    // create a view, give it a color, and add it to the main view
    UIView *square = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    square.backgroundColor = [UIColor blackColor];
    [self.view addSubview:square];
    

    // alloc the UISnapBehavior, with the square view, to the point that was touched
    UISnapBehavior *snapBehavior = [[UISnapBehavior alloc] initWithItem:square snapToPoint:point];
    
    // damping controls the amount of oscillation  value between 0 and 1.
    //[snapBehavior setDamping:.1];
    
    // add the snap to the Animator
    [self.myAnimator addBehavior:snapBehavior];
    

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
