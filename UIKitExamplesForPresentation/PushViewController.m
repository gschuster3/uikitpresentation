//
//  PushViewController.m
//  UIKitExamplesForPresentation
//
//  Created by GeraldSchuster on 2/24/14.
//  Copyright (c) 2014 Gerald Schuster. All rights reserved.
//

#import "PushViewController.h"

@interface PushViewController ()
@property (nonatomic, strong) UIDynamicAnimator * myAnimator;
@end

@implementation PushViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Initialize the animator and set the reference view
    self.myAnimator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    // Create the view to be pushed
    UIView *squareOne = [[UIView alloc] initWithFrame:CGRectMake(100, 100, 100, 100)];

    // Color the view
    squareOne.backgroundColor = [UIColor orangeColor];

    // Add the view into the main view
    [self.view addSubview:squareOne];
    
    // Initialize the push behavior.  This is an instantaneous push.
    UIPushBehavior *myPush = [[UIPushBehavior alloc] initWithItems:@[squareOne] mode:UIPushBehaviorModeInstantaneous];
    
    // give the push a vector for direction
    myPush.pushDirection = CGVectorMake(.3, .3);
  
    // If not specificed, the push will apply the force at the center of the object, but you adjust that
    
    //UIOffset offset = UIOffsetMake(20, 5);
    //[myPush setTargetOffsetFromCenter:offset forItem:squareOne];
    
    // add the push behavior to the animator
    [self.myAnimator addBehavior:myPush];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
